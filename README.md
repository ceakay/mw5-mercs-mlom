# MW5 Load Order Tool Set (L.O.T.S)

![L.O.T.S](UI/LOTS_Logo2.png "L.O.T.S")

LOTS is a load order management tool for Mechwarrior 5 Mercenaries, made with simplicity in mind


## Using LOTS

This section describes how to use LOTS for basic operations. It assumes you have been 
supplied with a load order you wish to use.

![Mod Directory Selection](mdImages/modDirSelect.png "Dir Select")

Start by selecting the location of your mods Directory (not the one for Steam/Epic's workshop).
From the location of your MW5 install this should be `MechWarrior 5 Mercenaries\MW5Mercs\Mods`. if this does not 
exist, create it. Once selected LOTS will index all your installed mods.

Optionally if you have mods installed through the Steam Workshop or the EGS equivalent, use the second Mod 
directory selection to select it.

Once this is done, LOTS will remember these selections for the next time you use it.

next, select the Preset Load Orders Tab if not already selected (this is the default active tab when LOTS is opened).

Now select the load order you wish to activate from the drop-down.

Finally hit `Appply Load Order` to activate it.

Congrats, the load order has been applied and you can now jump into game and play your mods.

_Note: apply a load order will disable any mods not in that load order, you can prevent this by unchecking the `Disable Mods not in Load Order` checkbox
be warned that doing so may void support from mod authors if you experience issues_

## Importing Load Orders into LOTS 

If you have been given a load order file that you would like to use with LOTS, importing them into LOTS is simple!

take the `.json` file you have been given and drop it into the `savedLoadOrders` directory that LOTS created in the same 
location LOTS was run from. Then press the refresh button and LOTS will load it into itself.

## Making/Editing Load Orders

To make a custom load order or edit an existing one, select the `Create Load Orders` Tab.

Next Select the Load Order you wish to use as a starting point.

![Edit Load Order](mdImages/editOrder.png "Create/Edit Load Order")

to enable or disable a mod from the load order simply check/uncheck the enabled checkbox for that mod.
to change the order a mod loads in simply double the Load Order field for the mod and then input the number you wish.

**Note: MW5 Loads mods from low -> high, so a mod with a load order of 0 will load before a mod with an order of 1. 
This means in the event 2 or more mods change the same object, the one with the highest load order will be the winner**

**Note 2: Multiple mods can have the same load order value, in some cases this is even desirable**

You can also change the name of the load order here.

When you are finished making your changes you can choose to save and update the existing load order, or save this 
order as a brand new load order

**Note: load orders supplied by a mod cannot be overwritten, you must save your tweaks as a new Load Order**

after saving or updating a load order, it will be saved to your `savedLoadOrders` directory. to give this order to 
others for use, simply take the produced `.json` file and send it to your friends, followers or mod users.


## Load Order File Format

Load Order Files are JSON formatted files

```json
{
    "lotsApiVersion": 1,
    "name": "Test Config",
    "LoadEntries": [
        {
            "enabled": true,
            "loadOrder": 16,
            "modName": "JW Test Mod"
        },
        {
            "enabled": true,
            "loadOrder": 3,
            "modName": "JWTestMod-Gone"
        }
    ]
}
```
_example load order file_

### Load Order Object

This is the top-level object of the load order file. a Single file can only contain, 1 `Load Order object`

```json
{
    "lotsApiVersion": 1,
    "name": "",
    "LoadEntries": []
}
```
`lotsApiVersion` : this is the version of LOTS file format this file adheres to. LOTS should always support prior 
versions of the format, but will need to be updated if it encounters a newer format version than it is aware of.

`name`: the name of this load order, it must be unique, attempting to load 2 load orders with the same name will result 
in one not displaying

`LoadEntries` : an array of `Load Entry` objects that define which mods are used and what order they should be loaded in


### Load Entry Object

This describes a single mod in a load order.

```json
{
    "enabled": true,
    "loadOrder": 0,
    "modName": ""
}
```

`enabled` : whether or not this mod is enabled. _Note: LOTS will not put disabled mods into the Load Order, but if 
one has been manually edited, LOTS will respect it

`loadOrder` : the load that should be applied to this mod

`modName` : the name of the mod, this must match the `displayName` field in the mod's mod.json file

## Shipping Load Orders with your mod

For mod authors who wish to ship one or more load orders with their mods, LOTS allows for you to define 
load orders in your `mod.json`. Load Orders defined here are immutable, end users cannot modify them (they of course use
them as a starting point for a tweaked load order, but the ones defined here will always be available in their original
form for as long as the mod is installed)

to add one or more Load Orders to a mod.json, add a `lotsPresetData` field. this field should be an array type.
from there just copy the contents of a load order file into the array.

example with 2 load orders:
```json
{
    "displayName": "JW Test Mod",
    "version": "1.2a",
    "buildNumber": 1,
    "description": "Nothing!",
    "author": "JWolf",
    "authorURL": "",
    "defaultLoadOrder": 16,
    "gameVersion": "1.1.0",
    "lotsPresetData": [
      {
         "lotsApiVersion": 1,
         "name": "JW Test - Standalone",
         "LoadEntries": [
             {
                 "enabled": true,
                 "loadOrder": 16,
                 "modName": "JW Test Mod"
             }
         ]
     },
      {
         "lotsApiVersion": 1,
         "name": "JW Test - with Awesome mod",
         "LoadEntries": [
               {
                  "enabled": true,
                  "loadOrder": 16,
                  "modName": "JW Test Mod"
               },
               {
                  "enabled": true,
                  "loadOrder": 3,
                  "modName": "JWTestMod-Gone"
               }
         ]
      }
    ]
}
```
