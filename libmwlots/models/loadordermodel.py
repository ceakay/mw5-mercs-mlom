from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import time
import csv
from ..data import LoadOrder, LoadEntry, Mod
import pprint
import logging


class LoadOrderTableModel(QAbstractTableModel):
    """
    inventory table model

    """

    def __init__(self, bReadOnly, mods, logger, parent=None):
        super(LoadOrderTableModel, self).__init__(parent)
        self.Header = ['Enabled', 'Name', 'Version', 'Missing', 'Load-Order']
        self.loadOrder = LoadOrder()
        self.readOnly = bReadOnly
        self.modsAvailable = mods # type: dict[str, Mod]
        self.modKeys =sorted(self.modsAvailable.keys(), key=str.lower)
        self.missingMods = []
        self.MissingModBrush = QBrush()
        self.MissingModBrush.setColor(Qt.red)
        self.Logger = logger # type: logging.Logger

    def update_table(self, dataCollection, mods):
        """
        change the inventory data loaded into the model

        :param dataCollection:
        :type dataCollection: LoadOrder
        :return:
        :rtype:
        """

        self.beginResetModel()
        self.loadOrder = dataCollection
        self.loadOrder.sortEntries()
        self.modsAvailable = mods
        self.modKeys = sorted(self.modsAvailable.keys(), key=str.lower)
        self.missingMods = []
        for entry in self.loadOrder.entries:
            if entry.enabled and entry.modName not in self.modsAvailable:
                # we may only have the directory name of the mod in question, try mapping it
                for mod in self.modsAvailable:
                    modObj = self.modsAvailable[mod]
                    if modObj.name == entry.modName:
                        self.Logger.info(f'remapped: {entry.modName} -> {modObj.displayName}')
                        entry.modName = modObj.displayName
                        break
                else:
                    missingMod = Mod()
                    missingMod.name = entry.modName
                    missingMod.displayName = entry.modName
                    self.missingMods.append(missingMod)
        self.endResetModel()
        # print 'Table Update Op took {0:02f} seconds'.format(time.time() - fl_st)

    def refresh(self):
        """
        refresh the table

        :return:
        :rtype:
        """
        self.beginResetModel()
        self.endResetModel()

    def hasMissingMods(self):
        return len(self.missingMods) > 0

    def rowCount(self, *args, **kwargs):
        return len(self.modsAvailable) + len(self.missingMods)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        # record = self.loadOrder[index.row()]
        if self.readOnly:
            return super(LoadOrderTableModel, self).flags(index)
        if index.column() in [4]:
            flags = super(LoadOrderTableModel, self).flags(index)
            flags |= Qt.ItemIsEditable
            return flags
        elif index.column() in [0]:
            flags = super(LoadOrderTableModel, self).flags(index)
            flags |= Qt.ItemIsUserCheckable | Qt.ItemIsEditable
            return flags
        else:
            return super(LoadOrderTableModel, self).flags(index)


    def data(self, index, role):
        if not index.isValid():
            return None
        bMissing = False
        if index.row() >= len(self.modKeys):
            record = self.missingMods[index.row() - len(self.modKeys)]
            bMissing = True
        else:
            record = self.modsAvailable[self.modKeys[index.row()]]
        entry = self.loadOrder.getEntry(record.displayName)
        if bMissing and role == Qt.ForegroundRole:
            return self.MissingModBrush
        if not self.readOnly and role == Qt.CheckStateRole and index.column() == 0:
            if entry:
                if entry.enabled:
                    return Qt.Checked
            return Qt.Unchecked
        if role not in [Qt.DisplayRole, Qt.EditRole, Qt.ItemDataRole]:
            return None


        if index.column() == 0:
            if entry:
                return entry.enabled
            return False
        elif index.column() == 1:
            if record.displayName != '???':
                return record.displayName
            return record.name
        elif index.column() == 2:
            return record.version
        elif index.column() == 3:
            return bMissing
        elif index.column() == 4:
            if entry:
                return entry.loadOrder
            return -1
        else:
            return None

    def setData(self, index, value, role):
        if not index.isValid():
            return False
        try:
            if index.row() >= len(self.modKeys):
                record = self.missingMods[index.row() - len(self.modKeys)]
                bMissing = True
            else:
                record = self.modsAvailable[self.modKeys[index.row()]]
            entry = self.loadOrder.getEntry(record.displayName)
            int_value = int(value)
            if index.column() == 0:
                bValue = value == Qt.Checked
                if bValue:
                    if entry:
                        entry.enabled = bValue
                    else:
                        entry = LoadEntry()
                        entry.modName = record.displayName
                        entry.loadOrder = 0
                        entry.enabled = bValue
                        self.loadOrder.entries.append(entry)
                else:
                    if entry:
                        self.loadOrder.entries.remove(entry)
                self.dataChanged.emit(index, self.index(index.row(), 4))
                return True
            if index.column() == 4:
                if int_value < 0 or entry is None:
                    return False
                entry.loadOrder = int_value
                self.dataChanged.emit(index, index)
                return True
        except ValueError:
            pass
        return False

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None