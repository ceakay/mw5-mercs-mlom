from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from ..data import ETabs
from .basetabwidget import BaseTabWidget
from ..forms import Ui_Form_Presets
from ..models import LoadOrderTableModel, LoadOrderProxyModel
import json
import traceback
import uuid
import os
import os.path
import time


class PresetsTabController(BaseTabWidget, Ui_Form_Presets):

    Id = ETabs.Presets
    GenOpFailWarning = False

    def __init__(self, bkprocessor, logger, parent=None):
        """

        :param bkprocessor:
        :type bkprocessor: BkProcessor
        :param parent:
        :type parent:
        """
        super(PresetsTabController, self).__init__(bkprocessor, logger, parent)
        self.setupUi(self)
        self.setWindowTitle('Preset Load Orders')
        self.dataModel = LoadOrderTableModel(True, self.bkProcessor.mods, self.logger)

        self.proxyModel = LoadOrderProxyModel()
        self.proxyModel.setSourceModel(self.dataModel)

        self.tableView.setModel(self.proxyModel)
        self.tableView.verticalHeader().hide()
        for x in range(0, len(self.dataModel.Header)):
            self.tableView.horizontalHeader().setSectionResizeMode(x, QHeaderView.Stretch)
        self.tableView.setSortingEnabled(True)
        self.lOrders = []

        self.comboBox_loadOrders.currentIndexChanged.connect(self.loadCurrentOrder)
        self.pushButton_applyLoadOrder.pressed.connect(self.applyLoadOrder)
        self.bkProcessor.WriteLoadOrdersComplete.connect(self.genericOpFinished)
        self.reload()

    def genericCallback(self):
        QMessageBox.information(self, "Mod Order Applied", "Mod load order was successfully applied")

    def genericFailCallback(self):
        QMessageBox.warning(self, "Load Order Failed", "Load order failed to apply, check logs and mod directory is correct")

    def reload(self):
        self.lOrders = sorted(list(self.bkProcessor.presets.keys()), key=str.lower)
        cId = self.bkProcessor.currentLoadOrder
        self.comboBox_loadOrders.blockSignals(True)
        self.comboBox_loadOrders.clear()
        idx = 0
        cIdx = 0
        for lOrder in self.lOrders:
            self.comboBox_loadOrders.addItem(lOrder)
            if cId == lOrder:
                idx = cIdx
            cIdx += 1
        self.comboBox_loadOrders.setCurrentIndex(idx)
        self.comboBox_loadOrders.blockSignals(False)
        self.loadCurrentOrder()

    def loadCurrentOrder(self):
        if self.comboBox_loadOrders.currentText() in self.bkProcessor.presets and self.bkProcessor.initComplete:
            self.dataModel.update_table(self.bkProcessor.presets[self.comboBox_loadOrders.currentText()].clone(),
                                        self.bkProcessor.mods)
            if self.dataModel.hasMissingMods():
                QMessageBox.warning(self, 'Mods Missing', "Selected Load Order contains mods that are not present, this may cause issues.")

    def applyLoadOrder(self):
        if not self.checkBox_disableOthers.isChecked():
            message = QMessageBox()
            message.setWindowIcon(self.windowIcon())
            message.setIcon(QMessageBox.Question)
            message.setWindowTitle("May Void Support")
            message.setText("Warning, by leaving other mods enabled that are not in this load order, you may run into issues and void any support from the author(s)\n\nProceed?")
            message.setStandardButtons(QMessageBox.No | QMessageBox.Yes)
            buttonY = message.button(QMessageBox.Yes)
            buttonY.setText('Continue Anyways')
            buttonN = message.button(QMessageBox.No)
            buttonN.setText('No Thanks')
            message.setDefaultButton(buttonN)
            message.exec_()

            if message.clickedButton() == buttonN:
                return
        self.setupProgressDialog()
        self.bkProcessor.WriteLoadOrder.emit(self.comboBox_loadOrders.currentText(),
                                             self.checkBox_disableOthers.isChecked())
