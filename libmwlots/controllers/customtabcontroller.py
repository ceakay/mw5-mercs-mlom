from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from ..data import ETabs, LoadOrder
from .basetabwidget import BaseTabWidget
from ..forms import Ui_Form_customLoadOrder
from ..models import LoadOrderTableModel, LoadOrderProxyModel
import json
import traceback
import uuid
import os
import os.path
import time


class CustomOrderTabController(BaseTabWidget, Ui_Form_customLoadOrder):

    Id = ETabs.CustomOrder

    def __init__(self, bkprocessor, logger, parent=None):
        """

        :param bkprocessor:
        :type bkprocessor: BkProcessor
        :param parent:
        :type parent:
        """
        super(CustomOrderTabController, self).__init__(bkprocessor, logger, parent)
        self.setupUi(self)
        self.setWindowTitle('Create Load Orders')
        self.label_explain.setText("MW5 Loads Mods with load orders from low to high, \nthis means to mods with a higher load \norder can overwrite those with a \nlower load order")
        self.dataModel = LoadOrderTableModel(False, self.bkProcessor.mods, self.logger)

        self.proxyModel = LoadOrderProxyModel()
        self.proxyModel.setSourceModel(self.dataModel)

        self.tableView.setModel(self.proxyModel)
        self.tableView.verticalHeader().hide()
        for x in range(0, len(self.dataModel.Header)):
            self.tableView.horizontalHeader().setSectionResizeMode(x, QHeaderView.Stretch)
        self.lOrders = []
        self.tableView.setSortingEnabled(True)

        self.comboBox_loadOrders.currentIndexChanged.connect(self.loadCurrentOrder)
        self.pushButton_update.pressed.connect(self.savePressed)
        self.pushButton_saveAs.pressed.connect(self.saveAsPressed)
        self.pushButton_delete.pressed.connect(self.deletePressed)

        self.currentLoadOrder = None # type: LoadOrder
        self.overloadIndex = None
        self.reload()

    def reload(self):
        self.lOrders = sorted(list(self.bkProcessor.presets.keys()), key=str.lower)
        cId = self.bkProcessor.currentLoadOrder
        if self.overloadIndex is not None:
            cId = self.overloadIndex
            self.overloadIndex = None
        self.comboBox_loadOrders.blockSignals(True)
        self.comboBox_loadOrders.clear()
        idx = 0
        cIdx = 0
        for lOrder in self.lOrders:
            self.comboBox_loadOrders.addItem(lOrder)
            if cId == lOrder:
                idx = cIdx
            cIdx += 1
        self.comboBox_loadOrders.setCurrentIndex(idx)
        self.comboBox_loadOrders.blockSignals(False)
        self.loadCurrentOrder()

    def _saveLoadOrder(self, bNew):
        self.currentLoadOrder.name = self.lineEdit_name.text()
        self.currentLoadOrder.lotsApiVersion = self.bkProcessor.apiVersion
        if self.currentLoadOrder.name == "":
            QMessageBox.warning(self, 'Name Not Set',
                                "Load Order requires a unique name")
            return
        if bNew:
            if self.currentLoadOrder.name in self.bkProcessor.presets:
                QMessageBox.warning(self, 'Name Not Unique',
                                    "Load Order requires a unique name")
                return
            else:
                self.currentLoadOrder.setPath(os.path.join(self.bkProcessor.Settings.customLosCache, self.currentLoadOrder.filename))
            if self.currentLoadOrder.isPreset:
                self.currentLoadOrder.isPreset = False
        else:
            del(self.bkProcessor.presets[self.comboBox_loadOrders.currentText()])

        self.bkProcessor.presets[self.currentLoadOrder.name] = self.currentLoadOrder
        self.currentLoadOrder.toFile(self.currentLoadOrder.Path)
        QMessageBox.information(self, "Load Order Saved", f"Load Order was Saved to: {self.currentLoadOrder.Path}")
        self.overloadIndex = self.currentLoadOrder.name
        self.bkProcessor.WriteOrderComplete.emit(True)

    def deletePressed(self):
        if os.path.isfile(self.currentLoadOrder.Path):
            try:
                os.remove(self.currentLoadOrder.Path)
            except:
                trace = traceback.format_exc().split('\n')
                self.Logger.critical('Failed to delete Load Order!')
                for line in trace:
                    self.Logger.critical(line)
                QMessageBox.critical(self, "Failed to Delete", "Failed to delete Load Order, check file is permissions")
                return
        del(self.bkProcessor.presets[self.comboBox_loadOrders.currentText()])
        self.bkProcessor.WriteOrderComplete.emit(True)

    def savePressed(self):
        self._saveLoadOrder(False)

    def saveAsPressed(self):
        self._saveLoadOrder(True)

    def loadCurrentOrder(self):
        if self.comboBox_loadOrders.currentText() in self.bkProcessor.presets and self.bkProcessor.initComplete:
            self.currentLoadOrder = self.bkProcessor.presets[self.comboBox_loadOrders.currentText()].clone() # type: LoadOrder
            self.dataModel.update_table(self.currentLoadOrder,self.bkProcessor.mods)
            self.lineEdit_name.setText(self.currentLoadOrder.name)
            if self.dataModel.hasMissingMods():
                QMessageBox.warning(self, 'Mods Missing',
                                    "Selected Load Order contains mods that are not present, this may cause issues.")
            if self.currentLoadOrder.isPreset:
                self.pushButton_delete.setEnabled(False)
                self.pushButton_update.setEnabled(False)
            else:
                self.pushButton_delete.setEnabled(True)
                self.pushButton_update.setEnabled(True)