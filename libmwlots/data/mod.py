from .basejsonobject import BaseDataObject
from .loadorder import LoadOrder
import json
import io


class Mod:

    displayNameKey = "displayName"
    versionKey = "version"
    gameVersionKey = "gameVersion"
    loadOrderKey = "defaultLoadOrder"
    lotsPresetKey = "lotsPresetData"

    def __init__(self):
        self.rawFields = {}
        self.name = ""
        self.loadOrder = 0
        self.presets = [] # type: list[LoadOrder]
        self.version = "???"
        self.gameVersion = "???"
        self.displayName = "???"
        self.hasLoadOrder = False
        self.filePath = ""

    def readFromFile(self, filePath, name):
        m_file = io.open(filePath, 'rb')
        data = m_file.read()
        data = data.decode(BaseDataObject.getUtfEncoding(data))
        m_file.close()
        self.filePath = filePath
        self.rawFields = json.loads(data)
        self.name = name

        if self.loadOrderKey in self.rawFields:
            self.loadOrder = self.rawFields[self.loadOrderKey]
        if self.displayNameKey in self.rawFields:
            self.displayName = self.rawFields[self.displayNameKey]
        if self.versionKey in self.rawFields:
            self.version = self.rawFields[self.versionKey]
        if self.gameVersionKey in self.rawFields:
            self.gameVersion = self.rawFields[self.gameVersionKey]

        if self.lotsPresetKey in self.rawFields:
            self.presets = []
            for item in self.rawFields[self.lotsPresetKey]:
                preset = LoadOrder()
                success = preset.fromJson(item)
                preset.isPreset = True
                self.hasLoadOrder |= success
                self.presets.append(preset)

    def writeToFile(self):
        self.rawFields[self.loadOrderKey] = self.loadOrder
        strData = json.dumps(self.rawFields, indent=4, ensure_ascii=False)
        strData = strData.encode('utf-8')
        m_file = io.open(self.filePath, mode='wb')
        m_file.write(strData)
        m_file.flush()
        m_file.close()


