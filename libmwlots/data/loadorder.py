from .basejsonobject import BaseDataObject, SubObjectMap
import re


class LoadEntry(BaseDataObject):

    JSON_Type = 'LoadEntry'

    def __init__(self):
        self.modName = ""
        self.loadOrder = 0
        self.enabled = True

    @classmethod
    def sortKey(cls, item):
        return item.modName

    def clone(self):
        loe = LoadEntry()
        loe.modName = self.modName
        loe.loadOrder = self.loadOrder
        loe.enabled = self.enabled
        return loe


class LoadOrder(BaseDataObject):

    JSON_Type = "LoadOrder"

    _SubObjectMapping = [
        SubObjectMap(LoadEntry, '_entries', 'LoadEntries')
    ]

    def __init__(self):
        self.name = ""
        self.lotsApiVersion = 1
        self._entries = [] # type: list[LoadEntry]
        self._isPreset = False
        self._path = ""

    @property
    def filename(self):
       return re.sub(r'[-()"#/@;:<>{}`+=~|.!?,]', "", self.name).replace(" ", "_") + '.json'

    @property
    def entries(self):
        """

        :return:
        :rtype: list[LoadEntry]
        """
        return self._entries

    @property
    def isPreset(self):
        return self._isPreset

    @isPreset.setter
    def isPreset(self, bPreset):
        self._isPreset = bPreset

    @property
    def Path(self):
        return self._path

    def setPath(self, path):
        self._path = path

    def sortEntries(self):
        self._entries = sorted(self._entries, key=LoadEntry.sortKey)

    def getEntry(self, modName):
        for entry in self._entries:
            if entry.modName == modName:
                return entry
        return None

    def clone(self):
        lod = LoadOrder()
        lod.name = self.name
        lod.lotsApiVersion = self.lotsApiVersion
        lod._isPreset = self._isPreset
        lod._path = self.Path
        for entry in self._entries:
            lod.entries.append(entry.clone())
        return lod
