from .basejsonobject import BaseDataObject


class LotsSettings(BaseDataObject):

    JSON_Type = 'LotsSettings'

    def __init__(self):
        self.modsDirectory = ""
        self.workshopModsDirectory = ""
        self.customLosCache = "savedLoadOrders"
