@echo off
del ..\mlom.py
del ..\libmwlots\forms\presets.py
del ..\libmwlots\forms\customloadorder.py
del ..\stylesheets_rc.py
del ..\images_rc.py

pyside2-uic.exe -o ..\mlom.py .\mlom.ui
pyside2-uic.exe -o ..\libmwlots\forms\presets.py .\presets.ui
pyside2-uic.exe -o ..\libmwlots\forms\customloadorder.py .\customloadorder.ui

pyside2-rcc.exe -o ..\images_rc.py .\images.qrc
pyside2-rcc.exe -o ..\stylesheets_rc.py .\stylesheets.qrc